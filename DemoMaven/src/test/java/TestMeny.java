package demoMaven;

import static org.junit.Assert.*;

import java.text.NumberFormat;

import org.junit.Test;

public class TestMeny {

	NumberFormat numberFormat = NumberFormat.getInstance();

	// test area, class rektangel.
	@Test
	public void testAreaRektangel() throws Exception {

		// area; 12 * 2 = 24
		Rektangel area = new Rektangel(12, 2);

		assertEquals(24, area.avläsArea(0), 0);
	}

	@Test
	public void testOmkretsRektangel() throws Exception {
		// omkrets = (12*2) + (2*2)
		Rektangel o = new Rektangel(12, 2);

		assertEquals(28, o.avläsOmkrets(), 0);
	}

	@Test
	public void testAreaCirkel() throws Exception {
		// area = Math.PI * (radie * radie);

		Cirkel a = new Cirkel(12);
		double testA = 452.39;
		numberFormat.setMaximumFractionDigits(2);
		String tA = numberFormat.format(testA);
		assertEquals(tA, a.getArea());
	}

	@Test
	public void testOmkretsCirkel() throws Exception {

		Cirkel o = new Cirkel(12);
		double testO = 75.4;
		numberFormat.setMaximumFractionDigits(2);
		String tO = numberFormat.format(testO);

		assertEquals(tO, o.getOmkrets());

	}

	@Test
	public void testDiameterCirkel() throws Exception {

		Cirkel d = new Cirkel(12);
		double testD = 24.0;
		numberFormat.setMaximumFractionDigits(2);
		String tD = numberFormat.format(testD);

		assertEquals(tD, d.getDiameter());
	}

}
