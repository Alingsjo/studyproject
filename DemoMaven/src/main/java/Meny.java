package demoMaven;

import java.util.Scanner;

public class Meny {
	static Scanner sc = new Scanner(System.in);

	public void getMeny() {
	int avslut = 0;
		
		
		do {
			
			System.out.println("------------[Meny]--------------");
			System.out.println("V�lj figur genom att mata in siffra");
			System.out.println("[1] Cirkel");
			System.out.println("[2] Rektangel");
			System.out.println("[3] Kvadrat");
			System.out.println("[0] Avsluta progammet");
			System.out.println("--------------------------------");
            
			int nummer = sc.nextInt();
		
            //switch case 
			switch (nummer) {
           
			case 1:
				System.out.println("Skriv in cirkelns radie : ");
				double radie = sc.nextDouble();
				Cirkel cirkel = new Cirkel(radie);
				cirkel.getCirkel();

				break;
               
			case 2:
				System.out.println("Skriv in: ");
				System.out.println("Rektangel h�jd: ");
				double h�jd = sc.nextDouble();
				System.out.println("Rektangel bredd: ");
				double bredd = sc.nextDouble();

				Rektangel rektangel = new Rektangel(h�jd, bredd);
				rektangel.getRektangel();

				break;
               
			case 3:
				System.out.println("Skriv in: ");
				System.out.println("Kvadratens ena sida");
				double sida = sc.nextDouble();

				Kvadrat kvadrat = new Kvadrat(sida, sida);
				kvadrat.getKvadrat();

				break;
            
			case 0:
				System.out.println("Programmet avslutas");
				System.exit(0);

				break;
           
			default:

				System.out.println("Ogiltligt svar, F�rs�k igen.");

				break;

			}

		} while (avslut == 0);

		sc.close();
		System.exit(0);

	

	}

}
