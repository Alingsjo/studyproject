package demoMaven;

import java.text.NumberFormat;

public class Cirkel {
	NumberFormat numberFormat = NumberFormat.getInstance();

	private double radie;

	public Cirkel(double radie) {
		this.radie = radie;

	}


	public double avl�sArea(double area) {
		area = Math.PI * (radie * radie);
		

		return area;
	}

	public String getArea() {
		double area = avl�sArea(0);
		numberFormat.setMaximumFractionDigits(2);
		String areaCirkel = numberFormat.format(area);
		return areaCirkel;
	}

	public double avl�sOmkrets(double omkrets)

	{

		double diameter = 0;
		omkrets = Math.PI * avl�sDiameter(diameter);

		return omkrets;
	}

	public String getOmkrets() {
		double omkrets = avl�sOmkrets(0);
		numberFormat.setMaximumFractionDigits(2);
		String omkretsCirkel = numberFormat.format(omkrets);

		return omkretsCirkel;
	}

	public double avl�sDiameter(double diameter) {
		diameter = radie * 2;

		return diameter;
	}

	public String getDiameter() {
		double diameter = avl�sDiameter(0);
		numberFormat.setMaximumFractionDigits(2);
		String diameterCirkel = numberFormat.format(diameter);

		return diameterCirkel;
	}

	public String getCirkel() {

		avl�sArea(0);
		avl�sDiameter(0);
		avl�sOmkrets(0);
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
		String areaCirkel = numberFormat.format(avl�sArea(0));
		String diameterCirkel = numberFormat.format(avl�sDiameter(0));
		String omkretsCirkel = numberFormat.format(avl�sOmkrets(0));
		System.out.println("Area p� cirkel : " + areaCirkel);
		System.out.println("                                     ");
		System.out.println("Diameter p� cirkel: " + diameterCirkel);
		System.out.println("                                     ");
		System.out.println("Omkrets p� cirkel : " + omkretsCirkel);
		System.out.println("                                     ");
		return areaCirkel + diameterCirkel + omkretsCirkel;

	}

}
